ReadMe - Calendar App 
============================

Calendar App is the official calendar app for Ubuntu Touch (UT). Feel free to contribute and get contact, easiest on Matrix or Telegram or in the issue tracker here on gitlab.
The calendar app once followed a test driven development (TDD) where tests are written in parallel to feature implementation to help spot regressions easier. We "lost" the test during the transition from Canonical, but hopefully we bring them back soon.

Original Design Doc
===================

https://docs.google.com/presentation/d/14NIPecPFKb_8Ad3O4suEGJqVOw9bC4n0s63uWalbZ98/edit#slide=id.p

Launch
======
This application is implemented in QML/JS.
You need Qt5, the Ubuntu UI Toolkit and the qmlscene viewer in order to launch this application natively on the PC.

    $ cd calendar-app
    $ qmlscene qml/calendar.qml

You can install and run it on your UT device using `clickable`. Once you have installed `clickable` (you can get it with apt/ppa or pip), its as simple as:

    $ cd calendar-app
    $ clickable

If you want to try the Calendar App out on the PC, but you don't have the Ubuntu UI Toolkit installed (most likely when you're not running Ubuntu <= 16.04) you can run it in clickables docker container:

    $ cd calendar-app
    $ clickable desktop

Contributing
==========

All contributions are welcome, please see the information below for details of how you can help.

**Coding**
Please refer to the *README.developers* file for details on how to build the app and contribute code.

**Translation**
Help us translate the app to make it accessible to more people. Refer to our [translations](https://ubports.com/page/vvo-focus-languagetranslation) page for details.

**Ideas & bugs**
Please create a new issue in our [issue tracker](https://gitlab.com/ubports/apps/calendar-app/issues).

**Other**
Please go to UBPorts [contribution](https://ubports.com/page/vo-get-involved) page for more details on other ways to supports UBPorts.
